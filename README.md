# QiSession

npm-compatible package of [libqi-js](https://github.com/aldebaran/libqi-js)

See the official documentation at [http://doc.aldebaran.com/2-5/dev/js/index.html](http://doc.aldebaran.com/2-5/dev/js/index.html)

### Installation

```shell
npm install --save qi@"https://gitlab.com/uts-magic-lab/libqi-js.git"
```

### Usage

```javascript
var QiSession = require('qi');
var Session = QiSession(connected, disconnected, host);
```
